package net.chenlin.dp.ids.server.dao;

import net.chenlin.dp.ids.server.entity.PassportConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * ids配置mapper
 * @author zhouchenglin[yczclcn@163.com]
 */
@Mapper
public interface PassportConfigMapper {

    /**
     * 根据参数key查询配置项
     * @param macroKey
     * @return
     */
    PassportConfigDO getByKey(String macroKey);

}
